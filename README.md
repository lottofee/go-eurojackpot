go-eurojackpot
----

go-eurojackpot ist ein Kommandozeilenclient um die aktuellen Ergebnisse der letzten Eurojackpotziehung anzuzeigen.

Installation
----
```go get bitbucket.org/lottofee/go-eurojackpot```

Beispielanzeige der Ergebnisse
---
[https://www.lottozahlenonline.com/eurojackpot/](https://www.lottozahlenonline.com/eurojackpot/) 
